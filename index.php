<?php 
require_once("./assets/php/config.php");
require_once("./assets/php/connect.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ZMF Quote Generator</title>
<link rel="stylesheet" href="./assets/css/style.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="./assets/js/products.js"></script>
</head>
<body>
<div id="app">
    <header></header>
    <section id="content">
        <form id="productsForm" name="productsForm" action="products.php" method="post">
            
            <select id="products">
            <!--<option>test</option>-->
            <?php 
            
            $queryReadProducts = "SELECT * FROM `zmf_quoter_products`";
            
            $res = $mysqli->query($queryReadProducts);
            
            $res->data_seek(0);
            while ($row = $res->fetch_assoc()) {
                echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
                //echo " id = " . $row['id'] . " | name = " . $row['name'];
            }
            
            // 1 = select options
            // 2 = checkboxes
            // 3 = radio
            // 4 = text
            ?>
            </select>
            
            <div id="results">
                
                Loading...
                
            </div>
            
            <div id="calculated">
                
            </div>
            
        </form>
        
        
    </section>
    <footer></footer>
</div>
</body>
</html>