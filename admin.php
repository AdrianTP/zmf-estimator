<!DOCTYPE html>
<html lang="en">
<head>
<title>ZMF Quote Generator</title>
<link rel="stylesheet" href="./assets/css/style.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="./assets/js/script.js"></script>
</head>
<body>
<div id="app">
    <header></header>
    <section id="content">
        <h1>Login Form</h1>
        <form id="loginForm" name="loginForm" action="./assets/php/login.php" method="post">
            <p>
                <label for="username">Username: </label>
                <input type="text" name="username" id="username" maxlength="30" />
            </p>
            <p>
                <label for="password">Password: </label>
                <input type="password" name="password" id="password" maxlength="72" />
            </p>
            <p>
                <input type="submit" id="login" name="login" value="Log In" />
            </p>
        </form>
        <div id="message"></div><!--
        <h1>Registration Form</h1>
        <form id="registrationForm" name="registrationForm" action="./assets/php/register.php" method="post">
            <p>
                <label for="regUsername">Username: </label>
                <input type="text" name="regUsername" id="regUsername" maxlength="30" />
            </p>
            <p>
                <label for="regPassword1">Password: </label>
                <input type="password" name="regPassword1" id="regPassword1" maxlength="72" />
            </p>
            <p>
                <label for="regPassword2">Password Again: </label>
                <input type="password" name="regPassword2" id="regPassword2" maxlength="72" />
            </p>
            <p>
                <input type="submit" id="register" name="register" value="Register" />
            </p>
        </form>
        <div id="regMessage"></div>
        <h1>Change Password</h1>
        <form id="passwordForm" name="passwordForm" action="./assets/php/change.php" method="post">
            <p>
                <label for="oldPassword">Password: </label>
                <input type="password" name="oldPassword" id="oldPassword" maxlength="72" />
            </p>
            <p>
                <label for="newPassword1">Password: </label>
                <input type="password" name="newPassword1" id="newPassword1" maxlength="72" />
            </p>
            <p>
                <label for="newPassword2">Password Again: </label>
                <input type="password" name="newPassword2" id="newPassword2" maxlength="72" />
            </p>
            <p>
                <input type="submit" id="register" name="register" value="Register" />
            </p>
        </form>
        <div id="regMessage"></div>
        <h1>Forgot Password</h1>
        <form id="registrationForm" name="registrationForm" action="./assets/php/register.php" method="post">
            <p>
                <label for="regUsername">Username: </label>
                <input type="text" name="regUsername" id="regUsername" maxlength="30" />
            </p>
            <p>
                <label for="regPassword1">Password: </label>
                <input type="password" name="regPassword1" id="regPassword1" maxlength="72" />
            </p>
            <p>
                <label for="regPassword2">Password Again: </label>
                <input type="password" name="regPassword2" id="regPassword2" maxlength="72" />
            </p>
            <p>
                <input type="submit" id="register" name="register" value="Register" />
            </p>
        </form>
        <div id="regMessage"></div>-->
    </section>
    <footer></footer>
</div>
</body>
</html>