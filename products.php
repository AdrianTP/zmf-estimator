<?php 

$is_ajax = $_REQUEST['is_ajax'];
$product = $_REQUEST['product'];
if(isset($is_ajax) && $is_ajax)
{    
    require_once("./assets/php/config.php");
    
    $mysqli = new mysqli($host, $user, $pass, $dbName);
    if ($mysqli->connect_errno) {
        //echo "<p class='error'>Database connection failure. Please contact the administrator. Error info: " . $mysqli->connect_error . "</p>";
        echo "<p class='error'>Database connection failure. Please contact the administrator.</p>";
    } else {
        if ($stmt = $mysqli->prepare("SELECT * FROM `zmf_quoter_offerings` WHERE `productid` = ?")) {
            $stmt->bind_param("i", $product);
            $stmt->execute();
            $stmt->bind_result($dbOffId, $dbOffName, $dbOffType, $dbOffProductId);
            while($stmt->fetch()) {
                echo "<fieldset>";
                echo "<legend>" . $dbOffName . "</legend>";
                switch ($dbOffType) {
                    case "select":
                        $mysqli2 = new mysqli($host, $user, $pass, $dbName);
                        if ($stmt2 = $mysqli2->prepare("SELECT * FROM `zmf_quoter_options` WHERE `offeringid` = ?")) {
                            $stmt2->bind_param("i", $dbOffId);
                            $stmt2->execute();
                            $stmt2->bind_result($dbOptId, $dbOptLabel, $dbOptPrice, $dbOptDefault, $dbOptRecommended, $dbOptIncluded, $dbOptOfferingId);
                            $selectId = str_replace(" ", "", ucwords($dbOffName));
                            echo "<select id='" . $selectId . "' name='" . $selectId . "'>";
                            while($stmt2->fetch()) {
                                echo "<option ";
                                if ($dbOptDefault == 1) echo "selected ";
                                echo "data-price='" . $dbOptPrice . "' value='" . $dbOptLabel . "'>" . $dbOptLabel;
                                if ($dbOptRecommended) echo " (Recommended)";
                                echo "</option>";
                            }
                            echo "</select>";
                            $stmt2->close();
                        } else {
                            echo "Database error. Please contact the administrator.";
                        }
                        $mysqli2->close();
                        break;
                    case "radio":
                        $mysqli2 = new mysqli($host, $user, $pass, $dbName);
                        if ($stmt2 = $mysqli2->prepare("SELECT * FROM `zmf_quoter_options` WHERE `offeringid` = ?")) {
                            $stmt2->bind_param("i", $dbOffId);
                            $stmt2->execute();
                            $stmt2->bind_result($dbOptId, $dbOptLabel, $dbOptPrice, $dbOptDefault, $dbOptRecommended, $dbOptIncluded, $dbOptOfferingId);
                            $radioId = str_replace(" ", "", ucwords($dbOffName));
                            echo "<ul>";
                            while($stmt2->fetch()) {
                                echo "<li><label><input type='radio' id='" . $radioId . "' name='" . $radioId . "'";
                                if ($dbOptDefault == 1) echo "checked ";
                                echo "data-price='" . $dbOptPrice . "' value='" . $dbOptLabel . "' ";
                                echo "/>";
                                echo $dbOptLabel;
                                if ($dbOptRecommended) echo " (Recommended)";
                                echo "</label></li>";
                            }
                            echo "</ul>";
                            $stmt2->close();
                        } else {
                            echo "Database error. Please contact the administrator.";
                        }
                        $mysqli2->close();
                        break;
                    case "checkbox":
                        $mysqli2 = new mysqli($host, $user, $pass, $dbName);
                        if ($stmt2 = $mysqli2->prepare("SELECT * FROM `zmf_quoter_options` WHERE `offeringid` = ?")) {
                            $stmt2->bind_param("i", $dbOffId);
                            $stmt2->execute();
                            $stmt2->bind_result($dbOptId, $dbOptLabel, $dbOptPrice, $dbOptDefault, $dbOptRecommended, $dbOptIncluded, $dbOptOfferingId);
                            $checkId = str_replace(" ", "", ucwords($dbOffName));
                            echo "<ul>";
                            while($stmt2->fetch()) {
                                echo "<li><label>";
                                echo "<input type='checkbox' id='" . $checkId . "' name='" . $checkId . "'";
                                if ($dbOptDefault == 1) echo "checked disabled ";
                                echo "data-price='" . $dbOptPrice . "' value='" . $dbOptLabel . "' ";
                                echo "/>";
                                echo $dbOptLabel;
                                if ($dbOptRecommended) echo " (Recommended)";
                                if ($dbOptIncluded) echo " (Included)";
                                echo "</label></li>";
                            }
                            echo "</ul>";
                            $stmt2->close();
                        } else {
                            echo "Database error. Please contact the administrator.";
                        }
                        $mysqli2->close();
                        break;
                    case "text":
                        $mysqli2 = new mysqli($host, $user, $pass, $dbName);
                        if ($stmt2 = $mysqli2->prepare("SELECT * FROM `zmf_quoter_options` WHERE `offeringid` = ?")) {
                            $stmt2->bind_param("i", $dbOffId);
                            $stmt2->execute();
                            $stmt2->bind_result($dbOptId, $dbOptLabel, $dbOptPrice, $dbOptDefault, $dbOptRecommended, $dbOptIncluded, $dbOptOfferingId);
                            $checkId = str_replace(" ", "", ucwords($dbOffName));
                            echo "<ul>";
                            while($stmt2->fetch()) {
                                echo "<li><label>";
                                echo "<input type='text' id='" . $checkId . "' name='" . $checkId . "'";
                                echo "data-price='" . $dbOptPrice . "' ";
                                echo "/>";
                                echo $dbOptLabel;
                                if ($dbOptRecommended) echo " (Recommended)";
                                echo "</label></li>";
                            }
                            echo "</ul>";
                            $stmt2->close();
                        } else {
                            echo "Database error. Please contact the administrator.";
                        }
                        $mysqli2->close();
                        break;
                }
                echo "</fieldset>";
            }
            $stmt->close();
        } else {
            echo "<p class='error'>Query failed. Error info: (" . $mysqli->errno . ") " . $mysqli->error . "</p>";
            //echo "<p class='error'>Query failed for unknown reason. Please contract the administrator.";
        }
        $mysqli->close();
    }
}