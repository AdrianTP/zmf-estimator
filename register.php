<?php 

function createSalt(){
    $string = md5(uniqid(rand(), true));
    return substr($string, 0, 3);
}

$is_ajax = $_REQUEST['is_ajax'];
if(isset($is_ajax) && $is_ajax)
{
	$username = $_REQUEST['username'];
	$password1 = $_REQUEST['password1'];
    $password2 = $_REQUEST['password2'];
    
    if ($password1 != $password2) {
        echo "<p class='error'>Passwords do not match.</p>";
    } elseif (strlen($username) > 30) {
        echo "<p class='error'>Username is too long (max 30 characters -- including alphanumeric, spaces, and special characters).</p>";
    } elseif (strlen($username) < 3) {
        echo "<p class='error'>Username is too short (min 3 characters -- including alphanumeric, spaces, and special characters).</p>";
    } elseif (strlen($password1) > 72) {
        echo "<p class='error'>Password is too long (max 72 characters -- including alphanumeric, spaces, and special characters).</p>";
    } elseif (strlen($password1) < 8) {
        echo "<p class='error'>Password is too short (min 8 characters -- including alphanumeric, spaces, and special characters).</p>";
    } else {
        require_once("./config.php");
        
        $mysqli = new mysqli($host, $user, $pass, $dbName);
        if ($mysqli->connect_errno) {
            //echo "<p class='error'>Database connection failure. Please contact the administrator. Error info: " . $mysqli->connect_error . "</p>";
            echo "<p class='error'>Database connection failure. Please contact the administrator.</p>";
        } else {
            $username = $mysqli->real_escape_string($username);
            if ($stmt = $mysqli->prepare("SELECT `username` FROM `zmf_quoter_users` WHERE `username` = ?")) {
                $stmt->bind_param("s", $username);
                $stmt->execute();
                $stmt->bind_result($dbUsername);
                $stmt->fetch();
                if ($username == $dbUsername) {
                    echo "<p class='error'>User already exists.";
                } else {
                    $hash = hash('sha256', $password1);
                    $salt = createSalt();
                    $hash = hash('sha256', $salt . $hash);
                    if ($stmt = $mysqli->prepare("INSERT INTO `zmf_quoter_users` ( `username`, `password`, `salt` ) VALUES ( ?, ?, ? )")) {
                        $stmt->bind_param("sss", $username, $hash, $salt);
                        $stmt->execute();
                        $stmt->close();
                        echo "<p class='success'>Created user \"" . $username . "\".</p>";
                    } else {
                        echo "<p class='error'>User creation failed. Error info: (" . $mysqli->errno . ") " . $mysqli->error . "</p>";
                    }
                }
            }
            $mysqli->close();
        }
    }
}






