<?php 

require_once("./session.php");

function createSalt(){
    $string = md5(uniqid(rand(), true));
    return substr($string, 0, 3);
}

$is_ajax = $_REQUEST['is_ajax'];
if(isset($is_ajax) && $is_ajax)
{
	session_start();
    $username = $_REQUEST['username'];
	$password = $_REQUEST['password'];
    
    require_once("./config.php");
    
    $mysqli = new mysqli($host, $user, $pass, $dbName);
    if ($mysqli->connect_errno) {
        //echo "<p class='error'>Database connection failure. Please contact the administrator. Error info: " . $mysqli->connect_error . "</p>";
        echo "<p class='error'>Database connection failure. Please contact the administrator.</p>";
    } else {
        $username = $mysqli->real_escape_string($username);
        if ($stmt = $mysqli->prepare("SELECT `password`, `salt` FROM `zmf_quoter_users` WHERE `username` = ?")) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $stmt->bind_result($dbPassword, $dbSalt);
            $stmt->fetch();
            $hash = hash('sha256', $dbSalt . hash('sha256', $password));
            if ($hash != $dbPassword) {
                echo "<p class='error'>Incorrect username and/or password.";
            } else {
                echo "<p class='success'>Welcome, " . $username . "!</p>";
                //$user->authorize();
                authorize();
            }         
            $stmt->close();
        } else {
            //echo "<p class='error'>Login failed. Error info: (" . $mysqli->errno . ") " . $mysqli->error . "</p>";
            echo "<p class='error'>Login failed for unknown reason. Please contact the administrator.</p>";
        }
        $mysqli->close();
    }
}