<?php 

require_once("./config.php");
require_once("./connect.php");

$queryCreateUsers = "CREATE TABLE IF NOT EXISTS `zmf_quoter_users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(30) NOT NULL UNIQUE,
    `password` VARCHAR(64) NOT NULL,
    `salt` VARCHAR(3) NOT NULL,
    PRIMARY KEY(`id`)
);";
$queryCreateProducts = "CREATE TABLE IF NOT EXISTS `zmf_quoter_products` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(30) NOT NULL UNIQUE,
    PRIMARY KEY(`id`)
);";
$queryCreateOfferings = "CREATE TABLE IF NOT EXISTS `zmf_quoter_offerings` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(30) NOT NULL UNIQUE,
    `type` INT NOT NULL,
    `productid` INT NOT NULL,
    PRIMARY KEY(`id`)
);";
$queryCreateOptions = "CREATE TABLE IF NOT EXISTS `zmf_quoter_options` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `label` VARCHAR(120) NOT NULL,
    `price` INT NOT NULL,
    `default` TINYINT(1) NOT NULL,
    `recommended` TINYINT(1) NOT NULL,
    `included` TINYINT(1) NOT NULL,
    `offeringid` INT NOT NULL,
    PRIMARY KEY(`id`)
);";

if ($mysqli->query($queryCreateUsers)) {
    echo "Users table created or exists.<br />";
} else {
    echo "Users table creation failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br />";
}

if ($mysqli->query($queryCreateProducts)) {
    echo "Products table created or exists.<br />";
} else {
    echo "Products table creation failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br />";
}

if ($mysqli->query($queryCreateOfferings)) {
    echo "Offerings table created or exists.<br />";
} else {
    echo "Offerings table creation failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br />";
}

if ($mysqli->query($queryCreateOptions)) {
    echo "Options table created or exists.<br />";
} else {
    echo "Options table creation failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br />";
}
/*
if (!$mysqli->query($queryCreateUsers) ||
    !$mysqli->query($queryCreateProducts) ||
    !$mysqli->query($queryCreateOfferings) ||
    !$mysqli->query($queryCreateOptions)) {
    echo "Table creation failed: (" . $mysqli->errno . ") " . $mysqli->error;
} else {
    echo "Installation successful.";
}
*/






