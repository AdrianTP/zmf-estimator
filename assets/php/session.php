<?php 

class User {
    
    public function authorize() {
        session_regenerate_id ();
        $_SESSION['valid'] = 1;
        $_SESSION['userid'] = $userid;
    }
    
    public function isLoggedIn() {
        if(isset($_SESSION['valid']) && $_SESSION['valid']) {
            return true;
        }
        return false;
    }
    
    public function deauthorize() {
        $_SESSION = array();
        session_destroy();
    }
    
}

$user = new User();

function authorize() {
    session_regenerate_id ();
    $_SESSION['valid'] = 1;
    $_SESSION['userid'] = $userid;
}

function isLoggedIn() {
    if(isset($_SESSION['valid']) && $_SESSION['valid']) {
        return true;
    }
    return false;
}

function deauthorize() {
    $_SESSION = array();
    session_destroy();
}