$.fn.hasAttr = function(name) {  
   return (typeof(this.attr(name)) !== "undefined" && this.attr(name) !== false);
};

$("#products").on("change.products", function() {
        
    var action = $("#productsForm").attr('action');
    var form_data = {
        product: $("#products").find(":selected").val(),
		is_ajax: 1
	};
	
	$.ajax({
		type: "POST",
		url: action,
		data: form_data,
		success: function(response) {
            $("#results").html(response);
		}
	});
	
    //console.log("product changed");
    
	return false;
});

var calculate = function() {
    var totalPrice = 0;
    $("select").not("#products").find(":selected").each(function() {
        totalPrice += $(this).data("price");
    });
    $("input[type='radio']:checked").each(function() {
        totalPrice += $(this).data("price");
    });
    $("input[type='checkbox']:checked").each(function() {
        totalPrice += $(this).data("price");
    });
    $("input[type='text']").each(function() {
        totalPrice += $(this).data("price") * $(this).val();
    });
    var calculated = "$" + totalPrice;
    $("#calculated").html(calculated);
};

$(document).on("change.inputs", "select, input, textarea", calculate);

$(document).ready(function() {
    
    var action = $("#productsForm").attr('action');
    var form_data = {
        product: $("#products").find(":selected").val(),
		is_ajax: 1
	};
	
	$.ajax({
		type: "POST",
		url: action,
		data: form_data,
		success: function(response) {
            $("#results").html(response);
            calculate();
		}
	});
    
});