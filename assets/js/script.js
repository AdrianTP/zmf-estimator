$(document).ready(function() {
    
	$("#login").on("click.login", function() {
	
		var action = $("#loginForm").attr('action');
		var form_data = {
			username: $("#username").val(),
			password: $("#password").val(),
			is_ajax: 1
		};
		
		$.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response) {
				$("#message").html(response);	
			}
		});
		
		return false;
	});
    
    $("#register").on("click.register", function() {
    
		var action = $("#registrationForm").attr('action');
		var form_data = {
			username: $("#regUsername").val(),
			password1: $("#regPassword1").val(),
            password2: $("#regPassword2").val(),
			is_ajax: 1
		};
		
		$.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response) {
                $("#regMessage").html(response);
			}
		});
		
		return false;
	});
});